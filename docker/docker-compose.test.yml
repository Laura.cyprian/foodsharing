version: '2'
services:

  # nginx
  #------------------------------------

  # proxies connections to app/chat

  web:
    container_name: foodsharing_test_web
    image: registry.gitlab.com/foodsharing-dev/images/web:1.8
    ports:
      - "127.0.0.1:28080:8080"
    expose:
      - 8080
    depends_on:
      - app
      - chat
    volumes:
      - ../:/app:delegated

  # main php app
  #------------------------------------

  app:
    container_name: foodsharing_test_app
    image: registry.gitlab.com/foodsharing-dev/images/php:4
    working_dir: /app
    expose:
      - 9000
    links:
      - redis:redis
      - db:db
      - chat:chat
      - influxdb
    depends_on:
      - db
      - redis
      - mailqueuerunner
    volumes:
      - ../:/app:delegated
      - selenium_downloads:/downloads:cached
      - /dev/null:/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini:delegated
    environment:
      FS_ENV: test
      REDIS_HOST: redis

  # webpack client javascript
  #------------------------------------

  client:
    container_name: foodsharing_test_client
    image: node:13.10.1-alpine
    # We don't run anything long running in here yet - build is run via ./scripts/build-assets
    command: 'true'
    working_dir: /app/client
    user: "${UID}"
    volumes:
      - ../:/app:delegated

  # php mail queue runner
  #------------------------------------

  mailqueuerunner:
    container_name: foodsharing_test_mailqueuerunner
    image: registry.gitlab.com/foodsharing-dev/images/php:4
    command: php run.php Mails queueWorker
    working_dir: /app
    restart: always
    depends_on:
      - db
      - redis
    links:
      - maildev
      - redis:redis
      - db:db
    volumes:
      - ../:/app:delegated
    environment:
      FS_ENV: test
      REDIS_HOST: redis

  chat:
    container_name: foodsharing_test_chat
    image: node:13.10.1-alpine
    command: sh -c "yarn && node server.js 0.0.0.0"
    working_dir: /app/chat
    depends_on:
      - redis
    environment:
      REDIS_HOST: redis
    expose:
      - 1337
      - 1338
    volumes:
      - ../:/app:delegated

  # mysql
  #------------------------------------

  db:
    container_name: foodsharing_test_db
    image: registry.gitlab.com/foodsharing-dev/images/db/test:1.4
    expose:
      - 3306
    ports:
      - "127.0.0.1:23306:3306"
    environment:
      MYSQL_ROOT_PASSWORD: root
      TZ: Europe/Berlin
    tmpfs:
      - /var/lib/mysql-tmpfs
    volumes:
      - ../:/app:delegated

  # redis
  #------------------------------------

  redis:
    container_name: foodsharing_test_redis
    image: redis:5.0.8-alpine
    expose:
      - 6379

  # selenium
  #------------------------------------

  # for running browser-based tests

  selenium:
    container_name: foodsharing_test_selenium
    image: registry.gitlab.com/foodsharing-dev/images/selenium:1.4
    shm_size: 512M
    links:
      - web:web
    expose:
      - 4444
    volumes:
      - selenium_downloads:/home/seluser/Downloads:cached

  # phpmyadmin
  #------------------------------------

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: foodsharing_test_phpmyadmin
    environment:
      PMA_HOST: db
      PMA_USER: root
      PMA_PASSWORD: root
    restart: always
    ports:
      - "127.0.0.1:28081:80"
    volumes:
      - /sessions

  # maildev
  #------------------------------------

  maildev:
    command: >
      bin/maildev
        --web 80
        --smtp 25
        --hide-extensions STARTTLS
    image: djfarrelly/maildev
    container_name: foodsharing_test_maildev
    ports:
      - "127.0.0.1:28084:80"

  influxdb:
    container_name: foodsharing_test_influxdb
    image: influxdb:latest
    command: >
      influxd -config /etc/influxdb/influxdb.toml
    ports:
    - "127.0.0.1:28086:8086"
    - "127.0.0.1:28089:8089/udp"
    environment:
      INFLUXDB_ADMIN_ENABLED: "true"
      INFLUXDB_DB: "foodsharing"
    volumes:
    - influxdb:/var/lib/influxdb:cached
    - ./conf/influxdb/influxdb.toml:/etc/influxdb/influxdb.toml:cached


volumes:
  selenium_downloads:
  influxdb:
