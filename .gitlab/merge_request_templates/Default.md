Closes ___ (e.g. #230)

## What does this MR do?

(briefly describe what this MR is about)

## How confident are you it won't break things if deployed?

(be honest!) 

## Links to related issues

Any relevant links (issues, documentation, slack discussions).

## How to test

Steps a reviewer can take to verify that this MR does what it says it does e.g.

1. Checkout branch locally
2. Login as foodsaver
3. ...

## Screenshots (if applicable)

Any relevant screenshots if this is a design / frontend change

## Checklist

- [ ] added a test, or explain why one is not needed/possible...
- [ ] no unrelated changes
- [ ] asked someone for a code review 
- [ ] joined #foodsharing-beta channel at https://slackin.yunity.org
- [ ] added an entry to CHANGELOG.md (description, merge request link, username(s))
- [ ] Once your MR has been merged, you are responsible to update the [#foodsharing-beta Slack channel](https://slackin.yunity.org/) about what has been changed here. They will test your work in different browsers, roles or other settings
